# Changelog

[TOC]

All notable changes to will be documented in this file.

## v3.0.0
See the git [diff](https://git.ufz.de/chs/progs/SMI/-/compare/v2.0.5...v3.0.0) for details.

### Enhancements
- SMI now depends on [FORCES](https://git.ufz.de/chs/forces/)
- cleanup repository
- added documentation page
- SMI can now be installed with `cmake` (see [INSTALL](doc/INSTALL.md))
- a command line interface was implemented to:
  - select the input namelist file
  - select the working directory
  - get help (`smi -h`) and version (`smi -v`) message
- new check-scripts to run checks in CI
- coverage calculation


## v2.0.5
See the git [diff](https://git.ufz.de/chs/progs/SMI/-/compare/v2.0.4...v2.0.5) for details.

### Changes
- Removed dependency on numerical recipes


## v2.0.4
See the git [diff](https://git.ufz.de/chs/progs/SMI/-/compare/v2.0.3...v2.0.4) for details.

### Enhancements
- includes flexible latlon coordinates (1D and 2D)


## v2.0.3
See the git [diff](https://git.ufz.de/chs/progs/SMI/-/compare/v2.0...v2.0.3) for details.

### Bugfixes
- openmp loop for SMI inversion now works correctly


## v2.0.2

### Bugfixes
- Check for monthly data covering multiple years is now correct.


## v2.0.1

### Enhancements
- implemented openmp parallelization for estimation of SMI values


## v2.0.0

### Enhancements
- consistent handling of leap days, no kde estimated for leap days,
  instead data of March 1st is used.
- soil moisture and kernel widths for kde are stored in one netcdf
  file called cdf_info.nc.
- added period object in mo_global_variables.
- mask file is now optional

### Bugfixes
- kde is correctly applied for sub-annual data
