# The Soil Moisture Index program - SMI

<div align="center">
<img src="https://git.ufz.de/chs/logos/-/raw/master/SMI.png" alt="LOGO" width="251px" style="width:251px;"/>
</div>

This repository contains the soil moisture index (SMI) Fortran program developed at the Dept. Computational Hydrosystems at the Helmholtz Centre for Environmental Research - UFZ.

The SMI comes with a LICENSE agreement, this includes also the GNU Lesser General Public License.

**Please note**: The GitLab repository grants read access to the code.
If you like to contribute to the code, please contact stephan.thober@ufz.de.

[TOC]

## Installation

Installation instructions can be found in [INSTALL](doc/INSTALL.md) for Windows, MacOS, and GNU/Linux distributions.

The simplest way to compile the SMI program is to use a [conda](https://docs.conda.io/en/latest/) environment (on Linux (including Windows/WSL) or MacOS)
provided by [Miniforge](https://github.com/conda-forge/miniforge):
```bash
conda create -y --prefix ./fortran_env
conda activate ./fortran_env
conda install -y git cmake make fortran-compiler netcdf-fortran
source CI-scripts/compile
```
This will give an executable `smi`.

## Usage

The SMI code uses a kernel density estimator with a gaussian kernel for transforming soil
moisture values to the soil moisture index (SMI). It has several
features that are described in the namelist file main.dat.

## Cite as

Please refer to the main model by citing Samaniego et al. (2013) and Samaniego et al. (2018):

- Samaniego et al. (2013), "Implications of Parameter Uncertainty on Soil Moisture Drought Analysis in Germany", J Hydrometeor, 2013 vol. 14 (1) pp. 47-68. http://journals.ametsoc.org/doi/abs/10.1175/JHM-D-12-075.1

- Samaniego et al. (2018), "Anthropogenic warming exacerbates European soil moisture droughts", Nature Climate change, 2018 pp. 1-9. http://dx.doi.org/10.1038/s41558-018-0138-5

## License

LGPLv3 (c) 2005-2024 CHS-Developers
